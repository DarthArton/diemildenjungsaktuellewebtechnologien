import React, { useState } from 'react';
import { Form, FormControl, Button } from 'react-bootstrap';

function ToDoForm({ addTodo }) {
    const [text, setText] = useState('');
    return (
        <Form data-testid="todo-form" onSubmit={(e) => {
            e.preventDefault();
            addTodo(text);
            setText('');
        }}>
            <Form.Group className="mb-3">
                <Form.Label>Add your Todos</Form.Label>
                <FormControl type="text" placeholder="Enter Todo" value={text} onChange={(e) => setText(e.target.value)} className="mb-3" />
                <Button variant="success" type="submit">Add Todo</Button>
            </Form.Group>
        </Form>
    );
}

export default ToDoForm;
