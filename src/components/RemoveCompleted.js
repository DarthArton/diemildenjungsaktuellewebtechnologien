import React from 'react';
import Button from 'react-bootstrap/Button';

function RemoveCompleted({ todos, removeCompleted }) {
    return (
        <Button variant="dark" onClick={() => removeCompleted(todos)}>Remove completed</Button>
    );
}

export default RemoveCompleted;
