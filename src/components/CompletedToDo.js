import React from 'react';
import { ListGroup, ListGroupItem, Button } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

function CompletedToDo({ todos, removeTodo }) {
    const completedTodos = todos.filter(todo => todo.completed);

    return (
        <div>
            <h3 style={{textAlign: 'center'}}>Completed To-Dos:</h3>
            <ListGroup>
                {completedTodos.map(todo => (
                    <ListGroupItem key={todo.id}>
                        <span>{todo.text}</span>
                        <Button variant="danger" onClick={() => removeTodo(todo.id)}>Remove</Button>
                    </ListGroupItem>
                ))}
            </ListGroup>
        </div>
    );
}

export default CompletedToDo;
