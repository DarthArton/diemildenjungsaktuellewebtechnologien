import React, { useState, useEffect } from 'react';
import ToDoForm from './ToDoForm';
import ToDoItem from './ToDoItem';
import CompletedToDo from './CompletedToDo';
import RemoveCompleted from './RemoveCompleted';
import { Container, Row, Col, Form, ListGroup, ListGroupItem } from 'react-bootstrap';
import Card from 'react-bootstrap/Card';

function ToDoList() {
    // State to hold the to-do items
    const [todos, setTodos] = useState([]);
    const [searchTerm, setSearchTerm] = useState('');

    // useEffect hook to retrieve the to-do items from the local storage on component mount
    useEffect(() => {
        const storedTodos = JSON.parse(localStorage.getItem('todos'));
        if (storedTodos) {
            setTodos(storedTodos);
        }
    }, []);

    // useEffect hook to update the local storage when the to-do items change
    useEffect(() => {
        localStorage.setItem('todos', JSON.stringify(todos));
    }, [todos]);

    const handleSearchChange = (event) => {
        setSearchTerm(event.target.value);
    }

    // function to add a new to-do item
    const addTodo = text => {
        const newTodo = { id: Date.now(), text, completed: false };
        setTodos([...todos, newTodo]);
    };

    // function to remove a to-do item
    const removeTodo = id => {
        setTodos(todos.filter(todo => todo.id !== id));
    };

    // function to toggle the completed state of a to-do item
    const toggleTodo = id => {
        setTodos(
            todos.map(todo => {
                if (todo.id === id) {
                    return { ...todo, completed: !todo.completed };
                }
                return todo;
            })
        );
    };

    const updateTodo = updatedTodo => {
        setTodos(
            todos.map(todo => {
                if (todo.id === updatedTodo.id) {
                    return updatedTodo;
                }
                return todo;
            })
        );
    }

    // function to remove all completed to-do items
    const removeCompleted = todos => {
        setTodos(todos.filter(todo => !todo.completed));
    };

    return (
        <Container>
            <Row>
                <Col md={{ span: 8, offset: 2 }}>
                    <Card>
                        <Card.Title> My To-Do List </Card.Title>
                        <Card.Subtitle className="mb-2 text-muted">Things you gotta do!</Card.Subtitle>
                        <Card.Body>
                            <ListGroup>
                            <ListGroupItem><ToDoForm addTodo={addTodo} testId="todo-form" /></ListGroupItem>
                            <ListGroupItem>{todos.filter(todo => todo.text.includes(searchTerm)).map(todo => (
                                <ToDoItem key={todo.id} todo={todo} removeTodo={removeTodo} toggleTodo={toggleTodo} updateTodo={updateTodo} />
                            ))} </ListGroupItem>
                            </ListGroup>
                        </Card.Body>
                        <Card.Body></Card.Body>
                        <Card.Footer>
                            <small className="text-muted">Search your Todos.</small>
                            <Form.Control type="text" placeholder="Search" value={searchTerm} onChange={handleSearchChange} />
                        </Card.Footer>
                    </Card>
                </Col>
            </Row>
            <Row>
                <Col md={{ span: 8, offset: 2 }}>
                    <Card>
                        <Card.Body>
                            <CompletedToDo todos={todos} removeTodo={removeTodo} />
                            <RemoveCompleted todos={todos} removeCompleted={removeCompleted} />
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    );
}

export default ToDoList;
