import React from 'react';
// import header from react bootstrap
import { Navbar } from 'react-bootstrap';

function Header() {
    return (
        <Navbar bg="dark" variant="dark">
            <Navbar.Brand>
                DieMildenJungs
            </Navbar.Brand>
        </Navbar>
    );
}

export default Header;