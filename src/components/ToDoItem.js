import React, { useState } from 'react';
import { Button, ListGroup, Form, FormControl } from 'react-bootstrap';

function ToDoItem({ todo, removeTodo, toggleTodo, updateTodo }) {
    const [isEditing, setIsEditing] = useState(false);
    const [newText, setNewText] = useState(todo.text);
    const [notes, setNotes] = useState(todo.notes);
    const [isAddingNotes, setIsAddingNotes] = useState(false);

    const handleSave = (e) => {
        e.preventDefault();
        const updatedTodo = { ...todo, text: newText };
        updateTodo(updatedTodo);
        setIsEditing(false);
    }

    const handleNotesSave = (e) => {
        e.preventDefault();
        updateTodo({ ...todo, notes });
        setIsAddingNotes(false);
    }

    return (
        <ListGroup.Item>
        {isEditing ? (
            <form onSubmit={handleSave}>
                <input type="text" value={newText} onChange={(e) => setNewText(e.target.value)}/>
                <Button variant="success" type="submit">Save</Button>
            </form>
        ) : (
            <>
                <input type="checkbox" checked={todo.completed} onChange={() => toggleTodo(todo.id)}/>
                <span style={{ textDecoration: todo.completed ? 'line-through' : 'none' }}>{todo.text}</span>
                <Button style={{margin: "3px"}} variant="danger" onClick={() => removeTodo(todo.id)}>Remove</Button>
                <Button style={{margin: "3px"}} variant="warning" onClick={() => setIsEditing(true)}>Edit</Button>
                <Button style={{margin: "3px"}} variant="info" onClick={() => setIsAddingNotes(!isAddingNotes)}>Add/View Notes</Button>
                {isAddingNotes && (
                    <Form onSubmit={handleNotesSave}>
                        <FormControl as="textarea" rows="3" value={notes} onChange={(e) => setNotes(e.target.value)}/>
                        <Button variant="success" type="submit">Save Notes</Button>
                    </Form>
                )}
            </>
        )}
    </ListGroup.Item>
    )
}

export default ToDoItem;
