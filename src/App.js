import './App.css';
import React, { Component } from 'react';
import ToDoList from './components/ToDoList';
import Header from './components/Header';

class App extends Component {
  render() {
      return <div className='App'>
          <Header />
        <ToDoList testId="todo-list"/>
        </div>;
  }
}

//ReactDOM.render(<App />, document.getElementById('root'));

export default App;
