import { render, screen, fireEvent } from '@testing-library/react';
import App from './App';
import ToDoForm from './components/ToDoForm';
import ToDoItem from './components/ToDoItem';
import ToDoList from './components/ToDoList';

test('renders my Header', () => {
  render(<App />);
  const toDoList = screen.getByText(/DieMildenJungs/i);
  expect(toDoList).toBeInTheDocument();
});

describe('ToDoForm', () => {
  it('renders the form', () => {
    render(<ToDoForm />);
    const input = screen.getByPlaceholderText(/Enter Todo/i);
    expect(input).toBeInTheDocument();
  });

  it('calls the addTodo function when the form is submitted', () => {
    const addTodo = jest.fn();
    render(<ToDoForm addTodo={addTodo} />);
    const input = screen.getByPlaceholderText(/Enter Todo/i);
    fireEvent.change(input, { target: { value: 'Test todo' } });
    const button = screen.getByText(/Add Todo/i);
    fireEvent.click(button);
    expect(addTodo).toHaveBeenCalledWith('Test todo');
  });

  it('clears the input field after the form is submitted', () => {
    render(<ToDoForm addTodo={() => {}} />);
    const input = screen.getByPlaceholderText(/Enter Todo/i);
    fireEvent.change(input, { target: { value: 'Test todo' } });
    const button = screen.getByText(/Add Todo/i);
    fireEvent.click(button);
    expect(input.value).toBe('');
  });
});

describe('ToDoItem', () => {
  it('renders the todo item with the correct text', () => {
    const todo = { id: 1, text: 'Test todo', completed: false };
    render(<ToDoItem todo={todo} removeTodo={() => {}} toggleTodo={() => {}} />);
    const todoText = screen.getByText(/Test todo/i);
    expect(todoText).toBeInTheDocument();
  });

  it('renders the todo item with the correct completed status', () => {
    const todo = { id: 1, text: 'Test todo', completed: true };
    render(<ToDoItem todo={todo} removeTodo={() => {}} toggleTodo={() => {}} />);
    const checkbox = screen.getByRole('checkbox');
    expect(checkbox.checked).toBe(true);
  });

  it('calls the toggleTodo function when the checkbox is clicked', () => {
    const toggleTodo = jest.fn();
    const todo = { id: 1, text: 'Test todo', completed: false };
    render(<ToDoItem todo={todo} removeTodo={() => {}} toggleTodo={toggleTodo} />);
    const checkbox = screen.getByRole('checkbox');
    fireEvent.click(checkbox);
    expect(toggleTodo).toHaveBeenCalledWith(1);
  });

  it('calls the removeTodo function when the remove button is clicked', () => {
    const removeTodo = jest.fn();
    const todo = { id: 1, text: 'Test todo', completed: false };
    render(<ToDoItem todo={todo} removeTodo={removeTodo} toggleTodo={() => {}} />);
    const removeButton = screen.getByText(/Remove/i);
    fireEvent.click(removeButton);
    expect(removeTodo).toHaveBeenCalledWith(1);
  });
});

describe('ToDoList', () => {
  it('renders the todo form', () => {
      render(<ToDoList />);
      const form = screen.getByTestId('todo-form');
      expect(form).toBeInTheDocument();
  });

  it('adds a todo when the form is submitted', () => {
      render(<ToDoList />);
      const input = screen.getByPlaceholderText('Enter Todo');
      fireEvent.change(input, { target: { value: 'Test todo' } });
      const form = screen.getByTestId('todo-form');
      fireEvent.submit(form);
      const todo = screen.getByText('Test todo');
      expect(todo).toBeInTheDocument();
  });
});
