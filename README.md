# Todo List App

This is a simple Todo List application built with React.

## Getting Started

1. Clone the repository: `git clone https://gitlab.com/DarthArton/diemildenjungsaktuellewebtechnologien.git`
2. Install dependencies: `npm install`
3. Start the development server: `npm start`
4. Open `http://localhost:3000` in your browser to see the app running.

## Features

- Add new Todo items
- Mark Todo items as completed
- Edit and update Todo items
- Add comments or notes to Todo items
- Remove Todo items
- Remove all Marked Todo items at once

## Dependencies

- React
- React-Bootstrap
- react-dom
- react-scripts

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.